# README #

### What is this repository for? ###

A Semantic Analyser for a programming language 

### How do I get set up? ###

To Compile the source code :
```bash
$ javac src/*  
```

To run 
```
$ java MicroPL86 <fileName>
```


### Who do I talk to? ###

[@tebesfinwo-admin](https://bitbucket.org/tebesfinwo-admin)