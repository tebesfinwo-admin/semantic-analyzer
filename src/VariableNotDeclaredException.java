/**
 * Created by tebesfinwo on 11/26/14.
 */
public class VariableNotDeclaredException extends Exception {

    public VariableNotDeclaredException(String variable) {
        super(variable + " is not declared. Please check your code");
    }
}
