/**
 * Created by tebesfinwo on 11/24/14.
 */
public class ParserException extends Exception {

    public ParserException(Lexer lexer, String message) {
        super("At line " + lexer.line + " col " + lexer.col + ": " + message);
    }
}
