/**
 * Created by tebesfinwo on 11/26/14.
 */
public class VariableExistedException extends Exception {

    public VariableExistedException(String variable) {
        super(variable + " has already been declared");
    }
}
