import java.io.IOException;

/**
 * Created by tebesfinwo on 11/24/14.
 *
 */
public class Parser {

    Lexer lexer;
    Token previousToken = null;
    Token currentVarToken = null;
    Token operationToken = null;
    boolean isAssignmentStatement = false;
    private SymbolTable symbolTable = new SymbolTable();

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }

    void parse() throws IOException, ParserException, VariableExistedException, VariableNotDeclaredException {
        this.program();
        this.match(Token.EOI);
    }

    private void program() throws IOException, ParserException, VariableExistedException, VariableNotDeclaredException {
        //System.out.println("Program");
        this.match(Token.PROGRAM);
        while (this.optMatch(Token.INT))
            this.declaration();
        while (!this.optMatch(Token.END))
            this.statement();
    }

    private void declaration() throws IOException, ParserException, VariableExistedException {
        //System.out.println("declaration");
        this.match(Token.ID);
        this.symbolTable.addVar2Table(this.previousToken.lexeme);
        while(this.optMatch(Token.COMMA)) {
            this.match(Token.ID);
            this.symbolTable.addVar2Table(this.previousToken.lexeme);
        }
        this.match(Token.SEMICOLON);
    }

    private void statement() throws IOException, ParserException, VariableNotDeclaredException {
        if ( this.optMatch(Token.ID) ) {
            this.currentVarToken = this.previousToken;
            this.assignmentStatement();
        }
        else if (this.optMatch(Token.IF))
            this.ifStatement();
        else if (this.optMatch(Token.WHILE))
            this.whileStatement();
        else if (this.optMatch(Token.FOR))
            this.forStatement();
        else if (this.optMatch(Token.PRINT))
            this.printStatement();
        else if (this.optMatch(Token.READ))
            this.readStatement();
        else if (this.optMatch(Token.LBRACE))
            this.compoundStatement();
        else
            throw new ParserException(lexer, "Expecting statement, found " + lexer.token());
    }

    private void assignmentStatement() throws IOException, ParserException, VariableNotDeclaredException {
        this.match(Token.ASSIGN_SYMBOL);
        this.isAssignmentStatement = true;
        this.expression();
        this.match(Token.SEMICOLON);
        this.isAssignmentStatement = false;
    }

    private void ifStatement() throws IOException, ParserException, VariableNotDeclaredException {
        this.match(Token.LPAREN);
        this.expression();
        this.relop();
        this.expression();
        this.match(Token.RPAREN);
        this.statement();
        if ( this.optMatch(Token.ELSE) ) {
            this.statement();
        }
    }

    private void whileStatement() throws IOException, ParserException, VariableNotDeclaredException {
        this.match(Token.LPAREN);
        this.expression();
        this.relop();
        this.expression();
        this.match(Token.RPAREN);
        this.statement();
    }

    private void forStatement() throws IOException, ParserException, VariableNotDeclaredException {
        this.match(Token.LPAREN);
        this.match(Token.ID);
        this.match(Token.ASSIGN_SYMBOL);
        this.expression();
        this.match(Token.COMMA);
        this.expression();
        this.match(Token.RPAREN);
        this.statement();
    }

    private void printStatement() throws IOException, ParserException, VariableNotDeclaredException {
        this.expression();
        this.match(Token.SEMICOLON);
    }

    private void readStatement() throws IOException, ParserException {
        this.match(Token.ID);
        this.match(Token.SEMICOLON);
    }

    private void compoundStatement() throws IOException, ParserException, VariableNotDeclaredException {
        while(!this.optMatch(Token.RBRACE))
            this.statement();
    }

    private void expression() throws IOException, ParserException, VariableNotDeclaredException {
        this.term();
        while ( this.optMatch(Token.ADD_OP) || this.optMatch(Token.MINUS_OP) ) {
            this.operationToken = this.previousToken;
            term();
        }
        this.operationToken = null;
    }

    private void term() throws IOException, ParserException, VariableNotDeclaredException {
        this.factor();
        while ( this.optMatch(Token.MUL_OP) || this.optMatch(Token.DIV_OP) || this.optMatch(Token.MOD_OP) ){
            this.operationToken = this.previousToken;
            this.factor();
        }
        this.operationToken = null;
    }

    private void factor() throws IOException, ParserException, VariableNotDeclaredException {
        if (this.optMatch(Token.ID)){
            this.assignVar();
            return;
        }
        else if (this.optMatch(Token.INT_LITERAL)){
            this.assignVar();
            return;
        }
        else if (this.optMatch(Token.LPAREN)){
            this.expression();
            this.match(Token.RPAREN);
        } else {
            throw new ParserException(this.lexer, "Expecting factor, found " + this.lexer.token());
        }
    }

    private void relop() throws ParserException, IOException {
        if (! this.lexer.token().isRelop())
            throw new ParserException(this.lexer, "Expecting relational, found " + this.lexer.token());
        this.lexer.next();
    }

    private boolean optMatch(int tokenType) throws IOException {
        if (lexer.token().type == tokenType){
            this.previousToken = lexer.token();
            lexer.next();
            return true;
        }
        return false;
    }

    private void match(int tokenType) throws ParserException, IOException {
        if (! this.optMatch(tokenType))
            throw new ParserException(this.lexer, "Expecting '" + new Token(tokenType) + "' , found " + lexer.token());
    }

    private void assignVar() throws VariableNotDeclaredException {
        if (! this.isAssignmentStatement)
            return;
        int currentVal;
        if ( this.previousToken.type == Token.ID )
            currentVal = this.symbolTable.getVal(this.previousToken.lexeme); //get value of the token
        else
            currentVal = Integer.parseInt(this.previousToken.lexeme);

        if ( this.operationToken == null) {
            this.symbolTable.assignVar(this.currentVarToken.lexeme, currentVal);
        } else {
            int currentVarVal = this.symbolTable.getVal(this.currentVarToken.lexeme); //current token value
            switch (this.operationToken.type) {
                case Token.ADD_OP :
                    this.symbolTable.assignVar(this.currentVarToken.lexeme, currentVarVal + currentVal);
                    break;
                case Token.MINUS_OP :
                    this.symbolTable.assignVar(this.currentVarToken.lexeme, currentVarVal - currentVal);
                    break;
                case Token.MOD_OP :
                    this.symbolTable.assignVar(this.currentVarToken.lexeme, currentVarVal % currentVal);
                    break;
                case Token.DIV_OP :
                    this.symbolTable.assignVar(this.currentVarToken.lexeme, currentVarVal / currentVal);
                    break;
                case Token.MUL_OP :
                    this.symbolTable.assignVar(this.currentVarToken.lexeme, currentVarVal * currentVal);
                    break;
            }
        }
    }


    public SymbolTable getSymbolTable(){
        return this.symbolTable;
    }

}
