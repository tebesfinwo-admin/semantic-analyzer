import java.util.Map;
import java.util.TreeMap;

/**
 * Created by tebesfinwo on 11/26/14.
 */
public class SymbolTable {

    private Map<String, Integer> varTable = new TreeMap<String, Integer>();

    /**
     * @param variable String
     * @exception if variable is existed in the variable table
     * */
    public void addVar2Table(String variable) throws VariableExistedException {
        if ( this.varTable.containsKey(variable) )
            throw new VariableExistedException(variable);
        else
            this.varTable.put(variable, 0);
    }

    public void assignVar(String variable, int value) throws VariableNotDeclaredException {
        if ( ! this.varTable.containsKey(variable) )
            throw  new VariableNotDeclaredException(variable);
        else
            this.varTable.put(variable, value);
    }

    public int getVal(String variable) throws VariableNotDeclaredException {
        if ( ! this.varTable.containsKey(variable) )
            throw  new VariableNotDeclaredException(variable);
        return this.varTable.get(variable);
    }

    @Override
    public String toString() {
        String message = "==== variables ==== \n";
        for (String key : this.varTable.keySet()){
            message = message + key + " : " + this.varTable.get(key) +"\n";
        }
        return message;
    }


}
