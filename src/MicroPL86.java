import java.io.File;
import java.io.IOException;

public class MicroPL86 {

    static String fileName = null;

    public static void main(String[] args) {

        processCommandLine(args);

        if (fileName == null){
            System.out.println("Usage: MicroPL86 <filename>");
            System.exit(2);
        }

        try {
            Parser parser = new Parser(new Lexer(new File(fileName)));
            parser.parse();
            System.out.println("*** Success ***");
            SymbolTable symbolTable = parser.getSymbolTable();
            System.out.println(symbolTable);
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (ParserException e) {
            System.err.println(e.getMessage());
        } catch (VariableExistedException e) {
            System.err.println(e.getMessage());
        } catch (VariableNotDeclaredException e) {
            System.err.println(e.getMessage());
        }

    }

    static void processCommandLine(String[] args){
        for(String arg : args){
            if (arg.startsWith("-")){

            }   else {
                fileName = arg;
            }
        }
    }
}
